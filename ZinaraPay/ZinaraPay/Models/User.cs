﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CompareObsolete = System.Web.Mvc.CompareAttribute;
namespace ZinaraPay.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        public string Username { get; set; }
        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        [Index(IsUnique = true)]
        public String Email { get; set; }
        [Required]
        public String Password { get; set; }
        [Display(Name = "ConfirmPassword")]
        [Required]
        [CompareObsolete("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public String ConfirmPassword { get; set; }
       public int LockCount { get; set; }
        public Boolean IsActive { get; set; }
        public DateTime? CreateDate { get; set; }
   
        public string role { get; set; }
        public virtual ICollection<Role> Roles { get; set; }

      
    }
}
