﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZinaraPay.Models
{
    public class Content 
    {     [Key]
        public int ID { get; set; }
        public string Title { get; set; }

        public byte[] Image { get; set; }
        public int UserID { get; set; }
       
    }

    public class Content2
    {
        [Key]
        public int ID { get; set; }
        public string Title { get; set; }

        public byte[] Image { get; set; }
        public int AlbumID { get; set; }

    }

    public class Content3
    {
        [Key]
        public int ID { get; set; }
        public string Title { get; set; }

        public byte[] Image { get; set; }
        public int VideoID { get; set; }

    }
}