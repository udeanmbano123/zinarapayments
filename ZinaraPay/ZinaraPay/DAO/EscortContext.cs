﻿using ZinaraPay.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using TrackerEnabledDbContext.Identity;

namespace ZinaraPay.DAO
{
    public class ZinaraPayContext : TrackerIdentityContext<ApplicationUser>
    {
        public ZinaraPayContext()
            : base("ZinaraPayContext")
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Content> Contents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.ToTable("UserRoles");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });

            base.OnModelCreating(modelBuilder);
        }

     



        //  public System.Data.Entity.DbSet<CDSLogin.Models.ApplicationUser> ApplicationUsers { get; set; }

    }



}